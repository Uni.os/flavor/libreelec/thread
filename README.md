# TV Box

Guide:
- https://forum.libreelec.tv/thread/12055-8-90-1-coreelec-for-s905-s912-devices/

obsolete:
- https://forum.libreelec.tv/thread/10612-8-90-11a-libreelec-9-x-for-s912/
- https://forum.libreelec.tv/thread/5556-howto-faq-install-community-builds-on-s905-s905d-s905w-s905x-s912-device/

Release:
https://github.com/CoreELEC/CoreELEC/releases